#-----------------------------------------------------------------------------------------------------
# GLOBAL VARIABLES
#-----------------------------------------------------------------------------------------------------

variable "project_name" {
  type = string
}

variable "environment" {
  type = string
  default = "default"
}

variable "aws_region" {
  type = string
}

variable "account_id" {
  type = string
  default = ""
}

# -----------------------------------------------------------------------------------------------------
# EC2 ROOT VOLUME VARIABLES
# -----------------------------------------------------------------------------------------------------

variable "vpc_id" {
  type = string
}

variable "sg_name" {
  type = string
}

variable "ingress" {
}
